package com.sd.careapp.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
public class Caregiver {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "birthdate")
    private String birthdate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "authorities_users", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "authority_id"))
    private Set<Authority> authority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Authority> getAuthority() {
        return authority;
    }

    public void setAuthority(Set<Authority> authority) {
        this.authority = authority;
    }
    /*
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((id == null) ? 0 : id.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            User other = (User) obj;
            if (id == null) {
                if (other.id != null)
                    return false;
            } else if (!id.equals(other.id))
                return false;
            return true;
        }
    */
    public Caregiver(){

    }

    public Caregiver(Long id, String username, String name, String gender, String birthdate, String address){
        this.id=id;
        this.username=username;
        this.name=name;
        this.gender=gender;
        this.birthdate=birthdate;
        this.address=address;
    }

    public Caregiver(Long id, String username, String password, String name, String gender, String birthdate, String address){
        this.id=id;
        this.username=username;
        this.password=password;
        this.name=name;
        this.gender=gender;
        this.birthdate=birthdate;
        this.address=address;
    }

    public Caregiver(String username, String name, String gender, String birthdate, String address){
        this.username=username;
        this.name=name;
        this.gender=gender;
        this.birthdate=birthdate;
        this.address=address;
    }


    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", password="  + "]";
    }

}
