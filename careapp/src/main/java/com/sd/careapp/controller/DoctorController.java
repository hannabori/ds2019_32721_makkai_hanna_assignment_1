package com.sd.careapp.controller;

import com.sd.careapp.entity.Caregiver;
import com.sd.careapp.entity.User;
import com.sd.careapp.repository.CaregiverRepository;
import com.sd.careapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.ValidationException;
import java.util.List;

@RestController
@RequestMapping(value = "/doctor")
@CrossOrigin
public class DoctorController {

    private UserRepository urp;


    @Autowired
    public DoctorController(UserRepository urp){
        this.urp = urp;
    }

    @RequestMapping(value = "/patients", method = RequestMethod.GET)
    public List<User> getAllPatients(){
        return  urp.findPatients();
    }

    @GetMapping(value = "/caregivers")
    public List<User> getAllCaregivers()
    {
        return urp.findCaregivers();
    }

    DoctorController(){

    }


}
