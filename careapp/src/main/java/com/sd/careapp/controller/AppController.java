package com.sd.careapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {
/*
	@GetMapping({"/","/login"})
	public String index() {
		return "index";
	}

	@GetMapping("/menu")
	public String menu() {
		return "menu";
	}
	*/

	@GetMapping("/patient")
	public String patient() {
		return "patient";
	}

	@GetMapping("/caregiver")
	public String caregiver() {
		return "caregiver";
	}
	
	@GetMapping("/doctor")
	public String doctor() { return "doctor";
	}
}
