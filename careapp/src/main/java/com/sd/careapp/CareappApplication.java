package com.sd.careapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class CareappApplication {

    public static void main(String[] args) {
        SpringApplication.run(CareappApplication.class, args);
    }

}
