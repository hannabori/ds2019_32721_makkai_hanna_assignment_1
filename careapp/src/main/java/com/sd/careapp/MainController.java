package com.sd.careapp;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {


    @GetMapping("/")
    public String root(){
        return "index";
    }


    @GetMapping("/login")
    public String login(Model model){
        return "index";
    }

    @GetMapping("/user")
    public String user(){
        return "user/index";
    }

}
